package com.example.alakadar;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Main2Activity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        z.start();
    }
    Thread z=new Thread(){
        @Override
        public void run() {
            try{
                sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                startActivity(new Intent(Main2Activity.this, Main3Activity.class));
                finish();
            }
        }
    };
}
