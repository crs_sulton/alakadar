package com.example.alakadar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME = "database.db";
    public static final String TABLE1_NAME = "training1_table";
    public static final String TABLE2_NAME = "training2_table";
    public String statusDaging="";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        Main5Activity m5 = new Main5Activity();
        statusDaging = m5.getStatusDaging();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql1 = "CREATE TABLE training1_table(id INTEGER PRIMARY KEY AUTOINCREMENT, meanh TEXT NULL, " +
                    "medianh TEXT NULL, modush TEXT NULL , varianh TEXT NULL, means TEXT NULL, medians TEXT NULL, " +
                    "moduss TEXT NULL, varians TEXT NULL,  meani TEXT NULL, mediani TEXT NULL, modusi TEXT NULL, " +
                    "variani TEXT NULL, contrast0 TEXT NULL, contrast45 TEXT NULL, contrast90 TEXT NULL, contrast135 TEXT NULL," +
                    "entropy0 TEXT NULL, entropy45 TEXT NULL, entropy90 TEXT NULL, entropy135 TEXT NULL, energy0 TEXT NULL, " +
                    "energy45 TEXT NULL, energy90 TEXT NULL, energy135 TEXT NULL, correlation0 TEXT NULL, correlation45 TEXT NULL, " +
                    "correlation90 TEXT NULL, correlation135 TEXT NULL, homogenity0 TEXT NULL, homogenity45 TEXT NULL, " +
                    "homogenity90 TEXT NULL, homogenity135 TEXT NULL, kelas TEXT NULL);";
        String sql2 = "CREATE TABLE training2_table(id INTEGER PRIMARY KEY AUTOINCREMENT, meanh TEXT NULL, " +
                    "medianh TEXT NULL, modush TEXT NULL , varianh TEXT NULL, means TEXT NULL, medians TEXT NULL, " +
                    "moduss TEXT NULL, varians TEXT NULL,  meani TEXT NULL, mediani TEXT NULL, modusi TEXT NULL, " +
                    "variani TEXT NULL, contrast0 TEXT NULL, contrast45 TEXT NULL, contrast90 TEXT NULL, contrast135 TEXT NULL," +
                    "entropy0 TEXT NULL, entropy45 TEXT NULL, entropy90 TEXT NULL, entropy135 TEXT NULL, energy0 TEXT NULL, " +
                    "energy45 TEXT NULL, energy90 TEXT NULL, energy135 TEXT NULL, correlation0 TEXT NULL, correlation45 TEXT NULL, " +
                    "correlation90 TEXT NULL, correlation135 TEXT NULL, homogenity0 TEXT NULL, homogenity45 TEXT NULL, " +
                    "homogenity90 TEXT NULL, homogenity135 TEXT NULL, kelas TEXT NULL);";

        Log.d("Data","onCreate: "+sql1);
        Log.d("Data","onCreate: "+sql2);
        db.execSQL(sql1);
        db.execSQL(sql2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        if (i==1) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE1_NAME);
            onCreate1(db);
        }else {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE2_NAME);
            onCreate2(db);
        }
    }
    public void onCreate2(SQLiteDatabase db) {
        String sql1 = "CREATE TABLE training2_table(id INTEGER PRIMARY KEY AUTOINCREMENT, meanh TEXT NULL, " +
                "medianh TEXT NULL, modush TEXT NULL , varianh TEXT NULL, means TEXT NULL, medians TEXT NULL, " +
                "moduss TEXT NULL, varians TEXT NULL,  meani TEXT NULL, mediani TEXT NULL, modusi TEXT NULL, " +
                "variani TEXT NULL, contrast0 TEXT NULL, contrast45 TEXT NULL, contrast90 TEXT NULL, contrast135 TEXT NULL," +
                "entropy0 TEXT NULL, entropy45 TEXT NULL, entropy90 TEXT NULL, entropy135 TEXT NULL, energy0 TEXT NULL, " +
                "energy45 TEXT NULL, energy90 TEXT NULL, energy135 TEXT NULL, correlation0 TEXT NULL, correlation45 TEXT NULL, " +
                "correlation90 TEXT NULL, correlation135 TEXT NULL, homogenity0 TEXT NULL, homogenity45 TEXT NULL, " +
                "homogenity90 TEXT NULL, homogenity135 TEXT NULL, kelas TEXT NULL);";

        Log.d("Data","onCreate: "+sql1);
        db.execSQL(sql1);
    }
    public void onCreate1(SQLiteDatabase db) {
        String sql1 = "CREATE TABLE training1_table(id INTEGER PRIMARY KEY AUTOINCREMENT, meanh TEXT NULL, " +
                "medianh TEXT NULL, modush TEXT NULL , varianh TEXT NULL, means TEXT NULL, medians TEXT NULL, " +
                "moduss TEXT NULL, varians TEXT NULL,  meani TEXT NULL, mediani TEXT NULL, modusi TEXT NULL, " +
                "variani TEXT NULL, contrast0 TEXT NULL, contrast45 TEXT NULL, contrast90 TEXT NULL, contrast135 TEXT NULL," +
                "entropy0 TEXT NULL, entropy45 TEXT NULL, entropy90 TEXT NULL, entropy135 TEXT NULL, energy0 TEXT NULL, " +
                "energy45 TEXT NULL, energy90 TEXT NULL, energy135 TEXT NULL, correlation0 TEXT NULL, correlation45 TEXT NULL, " +
                "correlation90 TEXT NULL, correlation135 TEXT NULL, homogenity0 TEXT NULL, homogenity45 TEXT NULL, " +
                "homogenity90 TEXT NULL, homogenity135 TEXT NULL, kelas TEXT NULL);";

        Log.d("Data","onCreate: "+sql1);
        db.execSQL(sql1);
    }
}
