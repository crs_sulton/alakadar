package com.example.alakadar;

import android.graphics.Bitmap;
import android.util.Log;

public class HSI {
    private Bitmap image;
    private int kolom;
    private int baris;
    private double mean;
    private double median;
    private double modus;
    private double varian;
    private int totalelemen;
    private double[][] Ht;
    private double[][] St;
    private double[][] It;
    private double[][] HSIt;
    public double[] fiturHSI;

    public HSI(Bitmap img){
        this.image = img;
        this.baris=img.getHeight();
        this.kolom=img.getWidth();
        totalelemen = baris * kolom;
        Ht = new double[baris][kolom];
        St = new double[baris][kolom];
        It = new double[baris][kolom];
        HSIt = new double[baris][kolom];
        getHSI(image);
        fiturHSI = new double[12];
    }
    public void getHSI(Bitmap b){
        int[] pix = new int[b.getHeight() * b.getWidth()];
        b.getPixels(pix, 0, b.getHeight(), 0, 0, b.getHeight(), b.getWidth());
        for (int x = 0; x < b.getWidth(); ++x)
            for (int y = 0; y < b.getHeight(); ++y)
            {
                int index = y * b.getHeight() + x;
                int R = (pix[index] >> 16) & 0xff;     //bitwise shifting
                int G = (pix[index] >> 8) & 0xff;
                int B = pix[index] & 0xff;

                double H = Math.acos((0.5*((R-G)+(R-B)))/((Math.sqrt(((R-G)*(R-G))+(R-B)*(G-B)))+0.00001));
                if (B>G){
                    H=(2*Math.PI)-H;
                }
                H=H/(2*Math.PI);
                double S =1-3*(Math.min(Math.min(R,G),B))/(R+G+B+0.00001);
                double I = (R+G+B)/3;

                int HSI = ((int) H << 16) + ((int)S << 8) + (int) I;
//                b.setPixel(x, y, HSI);
                Ht[x][y]= H;
                St[x][y]= S;
                It[x][y]= I;
                HSIt[x][y]=HSI;
//                Log.e("H", String.valueOf(Ht[x][y]));
//                Log.e("S", String.valueOf(St[x][y]));
//                Log.e("I", String.valueOf(It[x][y]));
            }
    }
    public void extract() {
        double m1 = getMean(Ht);
        double m2 = getMean(St);
        double m3 = getMean(It);
        double m4 = getMean(HSIt);

        fiturHSI[0] = m1;
        fiturHSI[1] = getMedian(Ht);
        fiturHSI[2] = getModus(Ht);
        fiturHSI[3] = getVarian(Ht,m1);

        fiturHSI[4] = m2;
        fiturHSI[5] = getMedian(St);
        fiturHSI[6] = getModus(St);
        fiturHSI[7] = getVarian(St,m2);

        fiturHSI[8] = m3;
        fiturHSI[9] = getMedian(It);
        fiturHSI[10] = getModus(It);
        fiturHSI[11] = getVarian(It,m3);

//        for (int i=0;i<12;i++) {
//            Log.e("FiturHSI", String.valueOf(fiturHSI[i]));
//        }

//        Log.e("mean1", String.valueOf("Mean : "+m1));
//        Log.e("mean2", String.valueOf("Mean : "+m2));
//        Log.e("mean3", String.valueOf("Mean : "+m3));

//        this.mean = (m3);
//        this.median = (getMedian(It));
//        this.modus = (getModus(It));
//        this.varian = (getVarian(It,m3));

    }
    public double getMean(double[][] matrix){
        double meane, sum=0;
        for (int i=0;i<matrix.length;i++){
            for (int j=0;j<matrix[0].length;j++){
                sum+=matrix[i][j];
            }
        }
        meane = sum / totalelemen;
        return meane;
    }
    public double getVarian(double[][] matrix,double m){
        double var = 0;
        for (int i=0;i<matrix.length;i++){
            for (int j=0;j<matrix[0].length;j++){
                var+=Math.pow((matrix[i][j]-m),2);
            }
        }
        var = Math.sqrt(var/(totalelemen-1));
        return var;
    }
    static void quickSort (double a[], int lo, int hi){
        //  lo adalah index bawah, hi adalah index atas
        //  dari bagian array yang akan di urutkan
        int i=lo, j=hi;
        double h;
        double pivot=a[lo];

        //  pembagian
        do{
            while (a[i]<pivot) i++;
            while (a[j]>pivot) j--;
            if (i<=j)
            {
                h=a[i]; a[i]=a[j]; a[j]=h;//tukar
                i++; j--;
            }
        } while (i<=j);

        //  pengurutan
        if (lo<j) quickSort(a, lo, j);
        if (i<hi) quickSort(a, i, hi);
    }

    public double getMedian(double[][] matrix){
        double med;
        int modulus = totalelemen % 2;
        double[]tmp = new double[matrix.length * matrix[0].length];
        int tengah,a=0,b=0;
        for (int i=0;i<matrix.length;i++){
            for (int j=0;j<matrix[0].length;j++){
                tmp[a]=matrix[i][j];
                a++;
            }
        }

        quickSort(tmp,0,((matrix.length * matrix[0].length)-1));

//        for (int i=0;i<matrix.length;i++){
//            for (int j=0;j<matrix[0].length;j++){
//                Log.e("urutan", String.valueOf((b+1)+" "+tmp[b]));
//                b++;
//            }
//        }

        if (modulus == 1){
            tengah = (totalelemen+1)/2;
        }else {
            tengah = totalelemen/2;
        }
        med = tmp[tengah-1];
        return med;
    }
    public double getModus(double[][] matrix){
        double[]tmp = new double[matrix.length * matrix[0].length];
        int a=0;
        for (int i=0;i<matrix.length;i++){
            for (int j=0;j<matrix[0].length;j++){
                tmp[a]=matrix[i][j];
                a++;
            }
        }
        int N=matrix.length * matrix[0].length;
        quickSort(tmp,0,(N-1));

        double HModus=0,FrekModus,KandidatModus,FrekKandidatModus;
        boolean MasihSama;
        FrekModus=0;
        int i=0;
        while(i < N){
            KandidatModus=tmp[i];
            FrekKandidatModus=1;
            i++;
            MasihSama = true;
            while(MasihSama && i<N){
                if(tmp[i]==KandidatModus){
                    FrekKandidatModus=FrekKandidatModus+1;
                    MasihSama=true;
                }
                else{
                    MasihSama=false;
                }
                i++;
            }
            if(FrekKandidatModus > FrekModus){
                HModus=KandidatModus;
                FrekModus=FrekKandidatModus;
            }
        }

//        double jumlahangka = 0;
//        double jumlahmodus = 0;
//        double modus = 0;
//        for (int i=0;i<matrix.length;i++){
//            for (int j=0;j<matrix[0].length;j++){
//                if (tmp[j] == tmp[i] && j != i) {
//                    jumlahangka++;
//                }
//            }
//            if (jumlahangka >= jumlahmodus) {
//                jumlahmodus = jumlahangka;
//                modus = tmp[i];
//                jumlahangka = 0;
//            }
//        }

        return HModus;
    }
//    public double getMeanq(){
//        return mean;
//    }
//    public double getModusq(){
//        return modus;
//    }
//    public double getMedianq(){
//        return median;
//    }
//    public double getVarianq(){
//        return varian;
//    }
    public double[] getFiturHSI(){
        return fiturHSI;
    }
}
