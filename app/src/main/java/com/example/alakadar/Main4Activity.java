package com.example.alakadar;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Main4Activity extends AppCompatActivity {
    ImageButton info;
    ImageButton cam;
    ImageButton sistem;
    ImageView slideImageView;


    private ViewPager mSlideViewPager;
    private LinearLayout mDotLayout;

    private TextView[] mDots;

    private SliderAdapter2 sliderAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        info = findViewById(R.id.image_info);
        cam = findViewById(R.id.image_cam);
        sistem = findViewById(R.id.image_sistem);
        slideImageView = findViewById(R.id.slide_image);

        mSlideViewPager = (ViewPager) findViewById(R.id.slideViewPager);
        mDotLayout = (LinearLayout) findViewById(R.id.dotsLayout);

        sliderAdapter = new SliderAdapter2(this);

        mSlideViewPager.setAdapter(sliderAdapter);
        addDots(0);
        mSlideViewPager.addOnPageChangeListener(viewListener);
    }
    public void addDots(int position){
        mDots=new TextView[5];
        mDotLayout.removeAllViews();
        for (int i=0; i<mDots.length;i++){
            mDots[i]=new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(R.color.colorPrimary));
            mDotLayout.addView(mDots[i]);
        }
        if (mDots.length>0){
            mDots[position].setTextColor(getResources().getColor(R.color.colorAccent));
        }
    }
    ViewPager.OnPageChangeListener viewListener=new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addDots(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    public void handleClickSistem(View view) {
        Intent intent = new Intent(Main4Activity.this, Main3Activity.class);
        startActivity(intent);
        finish();
    }
    public void handleClickCam(View view) {
        Intent intent1 = new Intent(Main4Activity.this, Main5Activity.class);
        startActivity(intent1);
        finish();
    }
    public void handleClickInfo(View view) {

    }
}
