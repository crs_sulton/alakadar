 package com.example.alakadar;

 import android.content.ActivityNotFoundException;
 import android.content.DialogInterface;
 import android.content.Intent;
 import android.database.Cursor;
 import android.database.sqlite.SQLiteDatabase;
 import android.graphics.Bitmap;
 import android.graphics.Color;
 import android.net.Uri;
 import android.os.Bundle;
 import android.os.Environment;
 import android.provider.MediaStore;
 import android.support.v7.app.AlertDialog;
 import android.support.v7.app.AppCompatActivity;
 import android.util.Log;
 import android.view.View;
 import android.widget.ImageButton;
 import android.widget.ImageView;
 import android.widget.TextView;
 import android.widget.Toast;

 import org.opencv.android.OpenCVLoader;
 import org.opencv.android.Utils;
 import org.opencv.core.Core;
 import org.opencv.core.CvType;
 import org.opencv.core.Mat;
 import org.opencv.core.Scalar;
 import org.opencv.imgcodecs.Imgcodecs;
 import org.opencv.ml.*;

 import java.io.ByteArrayOutputStream;
 import java.io.File;
 import java.io.FileNotFoundException;
 import java.io.FileOutputStream;
 import java.io.IOException;
 import java.nio.Buffer;
 import java.util.ArrayList;

 public class Main5Activity extends AppCompatActivity {
     DatabaseHelper myDb;
     protected Cursor cursor,cursor1;
     String[] training1,training2;
     String tamTraining1,tamTraining2;
     String statusDaging="";
     String tampung="";
     int kelas;
     View vi;
    ImageButton info;
    ImageButton cam;
    ImageButton sistem;
    public ImageView slide_image_crop;
    TextView entropy,energy,contras,correlation,dissimilarity,homogenity, kondisi;
    Uri uri;
    android.support.v7.widget.Toolbar toolbar;
    File file;
    Intent camera,gallery,crop;
    Bitmap bitmap,bitmapp,grayBitmap,bitmapHasil=null;
    private double[] allFiture;
    private double[] fitureHSI;
    private double[] fitureGLCM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        myDb = new DatabaseHelper(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        info = findViewById(R.id.image_info);
        cam = findViewById(R.id.image_cam);
        sistem = findViewById(R.id.image_sistem);
        slide_image_crop = findViewById(R.id.slide_image_crop);
        kondisi = findViewById(R.id.kondisi);

        if (!OpenCVLoader.initDebug())
            Log.d("ERROR", "Unable to load OpenCV");
        else
            Log.d("SUCCESS", "OpenCV loaded");

    }
    public void handleClickSistem(View view) {
        Intent intent = new Intent(Main5Activity.this, Main3Activity.class);
        startActivity(intent);
        finish();
    }
    public void handleClickCam(View view) {
//        SQLiteDatabase db = myDb.getWritableDatabase();
//        training1 = new String[360];
//        training2 = new String[360];
//        final CharSequence[] dialogitem = {"Lemari Es","Bukan Lemari Es"};
//        AlertDialog.Builder builder = new AlertDialog.Builder(Main5Activity.this);
//        builder.setTitle("Asal Daging");
//        builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int k) {
//                switch (k){
//                    case 0:
//                        cursor = db.rawQuery("select * from training2_table",null);
//                        cursor.moveToFirst();
//                        Log.d("Training",""+cursor.getCount());
//                        String txt="";
//                        for (int i=0;i<cursor.getCount();i++){
//                            cursor1 = db.rawQuery("select * from training2_table where id="+(i+1),null);
//                            cursor1.moveToPosition(0);
//                            training2[i]="";
//                            tamTraining2="";
//                            for (int j=1;j<34;j++) {
//                                tamTraining2+=" "+cursor1.getString(j).toString();
//                            }
//                            txt+= tamTraining2+"\n\n";
//                            training2[i]= tamTraining2;
//                        }
//                        Log.d("Training", txt);
//                        tampung=txt;
//                        file = new File(Environment.getExternalStorageDirectory(),
//                                "training2.txt");
//                        Save(file,tampung);
//                        break;
//                    case 1:
//                        cursor = db.rawQuery("select * from training1_table",null);
//                        cursor.moveToFirst();
//                        Log.d("Training",""+cursor.getCount());
//                        String txtt="";
//                        for (int i=0;i<cursor.getCount();i++){
//                            cursor1 = db.rawQuery("select * from training1_table where id="+(i+1),null);
//                            cursor1.moveToPosition(0);
//                            training1[i]="";
//                            tamTraining1="";
//                            for (int j=1;j<34;j++) {
//                                tamTraining1+=" "+cursor1.getString(j).toString();
//                            }
//                            txtt+= tamTraining1+"\n\n";
//                            training1[i]= tamTraining1;
//                        }
//                        Log.d("Training", txtt);
//                        tampung=txtt;
//                        file = new File(Environment.getExternalStorageDirectory(),
//                                "training1.txt");
//                        Save(file,tampung);
//                        break;
//                }
//            }
//        });
//        builder.create().show();
//        Log.d("Training", "Lokasi: "+getFilesDir());
    }
     public static void Save(File file, String data)
     {
         FileOutputStream fos = null;
         try
         {
             fos = new FileOutputStream(file);
         }
         catch (FileNotFoundException e) {e.printStackTrace();}
         try
         {
             try
             {
                 fos.write(data.getBytes());
             }
             catch (IOException e) {e.printStackTrace();}
         }
         finally
         {
             try
             {
                 fos.close();
             }
             catch (IOException e) {e.printStackTrace();}
         }
     }
    public void handleClickInfo(View view) {
        Intent intent1 = new Intent(Main5Activity.this, Main4Activity.class);
        startActivity(intent1);
        finish();
    }

     public void insertData(View view) {
         SQLiteDatabase db = myDb.getWritableDatabase();
         final CharSequence[] dialogitem = {"Daging Dingin","Daging Normal"};
         AlertDialog.Builder builder = new AlertDialog.Builder(Main5Activity.this);
         builder.setTitle("Jenis Daging");
         builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialogInterface, int i) {
                 switch (i){
                     case 0:
                         String sql = "insert into training2_table (meanh,medianh,modush,varianh,means,medians,moduss,varians," +
                                 "meani,mediani,modusi,variani,contrast0,contrast45,contrast90,contrast135,entropy0,entropy45,entropy90," +
                                 "entropy135,energy0,energy45,energy90,energy135,correlation0,correlation45,correlation90,correlation135," +
                                 "homogenity0,homogenity45,homogenity90,homogenity135,kelas) values ('"+allFiture[0]+"','"+allFiture[1]+"'" +
                                 ",'"+allFiture[2]+"','"+allFiture[3]+"','"+allFiture[4]+"','"+allFiture[5]+"','"+allFiture[6]+"'" +
                                 ",'"+allFiture[7]+"','"+allFiture[8]+"','"+allFiture[9]+"','"+allFiture[10]+"','"+allFiture[11]+"'" +
                                 ",'"+allFiture[12]+"','"+allFiture[13]+"','"+allFiture[14]+"','"+allFiture[15]+"','"+allFiture[16]+"'" +
                                 ",'"+allFiture[17]+"','"+allFiture[18]+"','"+allFiture[19]+"','"+allFiture[20]+"','"+allFiture[21]+"'" +
                                 ",'"+allFiture[22]+"','"+allFiture[23]+"','"+allFiture[24]+"','"+allFiture[25]+"','"+allFiture[26]+"'" +
                                 ",'"+allFiture[27]+"','"+allFiture[28]+"','"+allFiture[29]+"','"+allFiture[30]+"','"+allFiture[31]+"','"+1+"')";
                         db.execSQL(sql);
                         Log.d("Training", ""+sql);
                         break;
                     case 1:
                         sql = "insert into training1_table (meanh,medianh,modush,varianh,means,medians,moduss,varians," +
                                 "meani,mediani,modusi,variani,contrast0,contrast45,contrast90,contrast135,entropy0,entropy45,entropy90," +
                                 "entropy135,energy0,energy45,energy90,energy135,correlation0,correlation45,correlation90,correlation135," +
                                 "homogenity0,homogenity45,homogenity90,homogenity135,kelas) values ('"+allFiture[0]+"','"+allFiture[1]+"'" +
                                 ",'"+allFiture[2]+"','"+allFiture[3]+"','"+allFiture[4]+"','"+allFiture[5]+"','"+allFiture[6]+"'" +
                                 ",'"+allFiture[7]+"','"+allFiture[8]+"','"+allFiture[9]+"','"+allFiture[10]+"','"+allFiture[11]+"'" +
                                 ",'"+allFiture[12]+"','"+allFiture[13]+"','"+allFiture[14]+"','"+allFiture[15]+"','"+allFiture[16]+"'" +
                                 ",'"+allFiture[17]+"','"+allFiture[18]+"','"+allFiture[19]+"','"+allFiture[20]+"','"+allFiture[21]+"'" +
                                 ",'"+allFiture[22]+"','"+allFiture[23]+"','"+allFiture[24]+"','"+allFiture[25]+"','"+allFiture[26]+"'" +
                                 ",'"+allFiture[27]+"','"+allFiture[28]+"','"+allFiture[29]+"','"+allFiture[30]+"','"+allFiture[31]+"','"+1+"')";
                         db.execSQL(sql);
                         Log.d("Training", ""+sql);
                         break;
                 }
             }
         });
         builder.create().show();
     }
     public void trainingData(View view) {
//        SVMLatih latih = new SVMLatih();
//        latih.getData();
         SQLiteDatabase db = myDb.getWritableDatabase();
         for (int i=1;i<=120;i++) {
             uri = Uri.fromFile(new File("/storage/sdcard0/PKM_Latih/Tidak Segar/S/(" + i + ").JPG"));
             try {
                 bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(String.valueOf(uri)));
             } catch (Exception e) {
                 Log.e("Training", String.valueOf("Gagal : " + e +" "+ i));
             }
             slide_image_crop.setImageBitmap(bitmap);
             getFitur(bitmap);
             String sql = "insert into training1_table (meanh,medianh,modush,varianh,means,medians,moduss,varians," +
                     "meani,mediani,modusi,variani,contrast0,contrast45,contrast90,contrast135,entropy0,entropy45,entropy90," +
                     "entropy135,energy0,energy45,energy90,energy135,correlation0,correlation45,correlation90,correlation135," +
                     "homogenity0,homogenity45,homogenity90,homogenity135,kelas) values ('" + allFiture[0] + "','" + allFiture[1] + "'" +
                     ",'" + allFiture[2] + "','" + allFiture[3] + "','" + allFiture[4] + "','" + allFiture[5] + "','" + allFiture[6] + "'" +
                     ",'" + allFiture[7] + "','" + allFiture[8] + "','" + allFiture[9] + "','" + allFiture[10] + "','" + allFiture[11] + "'" +
                     ",'" + allFiture[12] + "','" + allFiture[13] + "','" + allFiture[14] + "','" + allFiture[15] + "','" + allFiture[16] + "'" +
                     ",'" + allFiture[17] + "','" + allFiture[18] + "','" + allFiture[19] + "','" + allFiture[20] + "','" + allFiture[21] + "'" +
                     ",'" + allFiture[22] + "','" + allFiture[23] + "','" + allFiture[24] + "','" + allFiture[25] + "','" + allFiture[26] + "'" +
                     ",'" + allFiture[27] + "','" + allFiture[28] + "','" + allFiture[29] + "','" + allFiture[30] + "','" + allFiture[31] + "','" + 2 + "')";
             db.execSQL(sql);
             Log.d("Training", "Training ke: " + i);
             Toast.makeText(this,"Training ke: "+Double.toString(i),Toast.LENGTH_SHORT).show();
         }
     }
     public void dropData(View view) {
         SQLiteDatabase db = myDb.getWritableDatabase();
         final CharSequence[] dialogitem = {"Lemari Es","Bukan Lemari Es"};
         AlertDialog.Builder builder = new AlertDialog.Builder(Main5Activity.this);
         builder.setTitle("Asal Daging");
         builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialogInterface, int i) {
                 switch (i){
                     case 0:
                         myDb.onUpgrade(db,0,0);
                         break;
                     case 1:
                         myDb.onUpgrade(db,1,0);
                         break;
                 }
             }
         });
         builder.create().show();
     }
    public void bukaKamera(View view) {
        final CharSequence[] dialogitem = {"Daging Dingin","Daging Normal"};
        AlertDialog.Builder builder = new AlertDialog.Builder(Main5Activity.this);
        builder.setTitle("Jenis Daging");
        builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case 0:
                        statusDaging="yes";
                        CameraOpen();
                        break;
                    case 1:
                        statusDaging="no";
                        CameraOpen();
                        break;
                }
            }
        });
        builder.create().show();
    }
    public void bukaGallery(View view) {
        final CharSequence[] dialogitem = {"Daging Dingin","Daging Normal"};
        AlertDialog.Builder builder = new AlertDialog.Builder(Main5Activity.this);
        builder.setTitle("Jenis Daging");
        builder.setItems(dialogitem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                switch (i){
                    case 0:
                        statusDaging="yes";
                        GalleryOpen();
                        break;
                    case 1:
                        statusDaging="no";
                        GalleryOpen();
                        break;
                }
            }
        });
        builder.create().show();
    }

    private void GalleryOpen() {
        gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(gallery,"Select image from gallery"),2);
    }
    private void CameraOpen() {
        camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(Environment.getExternalStorageDirectory(),
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        camera.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        camera.putExtra("return-data",true);
        startActivityForResult(camera,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==0 && resultCode==RESULT_OK){
            cropImage();
        }
        else if (requestCode==2){
            if (data != null){
                uri=data.getData();
                cropImage();
            }
        }
        else if (requestCode==1){
            if (data!=null){
                bitmapp=(Bitmap)data.getExtras().get("data");
                bitmap=(Bitmap)data.getExtras().get("data");
                slide_image_crop.setImageBitmap(bitmap);
                getFitur(bitmapp);
            }
        }
    }

    private void cropImage(){
        try {
            crop = new Intent("com.android.camera.action.CROP");
            crop.setDataAndType(uri,"image/*");

            crop.putExtra("crop","true");
            crop.putExtra("outputX",256);
            crop.putExtra("outputY",256);
            crop.putExtra("aspectX",1);
            crop.putExtra("aspectY",1);
            crop.putExtra("scale",true);
            crop.putExtra("return-data",true);

            startActivityForResult(crop,1);
        }
        catch (ActivityNotFoundException ex){

        }
    }
    public Bitmap getHSI(Bitmap b){
        int[] pix = new int[b.getHeight() * b.getWidth()];
        b.getPixels(pix, 0, b.getHeight(), 0, 0, b.getHeight(), b.getWidth());
        for (int x = 0; x < b.getWidth(); ++x)
            for (int y = 0; y < b.getHeight(); ++y)
            {
                int index = y * b.getHeight() + x;
                int R = (pix[index] >> 16) & 0xff;     //bitwise shifting
                int G = (pix[index] >> 8) & 0xff;
                int B = pix[index] & 0xff;

                double H = Math.acos((0.5*((R-G)+(R-B)))/((Math.sqrt(((R-G)*(R-G))+(R-B)*(G-B)))+0.00001));
                if (B>G){
                    H=(2*Math.PI)-H;
                }
                H=H/(2*Math.PI);
                double S =1-3*(Math.min(Math.min(R,G),B))/(R+G+B+0.00001);
                double I = (R+G+B)/3;

                int HSI = ((int) H << 16) + ((int)S << 8) + (int) I;
                b.setPixel(x, y, HSI);
            }
        return b;
    }
     public Bitmap getGRAY(Bitmap b){
        int baris = b.getHeight();
        int kolom = b.getWidth();
         int[] pix = new int[baris * kolom];
         b.getPixels(pix, 0, kolom, 0, 0, kolom, baris);

         for (int y = 0; y < baris; y++){
             for (int x = 0; x < kolom; x++)
             {
                 int index = y * b.getHeight() + x;
                 int R = (pix[index] >> 16) & 0xff;     //bitwise shifting
                 int G = (pix[index] >> 8) & 0xff;
                 int B = pix[index] & 0xff;

                 double gr = ((R*0.2989) + (G*0.5870) + (B*0.1140));
//                 Log.e("gray", String.valueOf(gr));
                 b.setPixel(x, y, (int)gr);
             }
         }
         return b;
     }
     public String getStatusDaging(){
        return statusDaging;
     }
    public void getFitur(Bitmap img){
        GLCM glcm = new GLCM(img,255);
        HSI hsi = new HSI(img);
        SVMLatih svm = new SVMLatih(statusDaging);
        glcm.extract();
        hsi.extract();

        allFiture = new double[33];
        fitureHSI = new double[12];
        fitureGLCM = new double[20];

        fitureHSI = hsi.getFiturHSI();
        fitureGLCM = glcm.getFiturGLCM();

        for (int i=0;i<33;i++){
            if (i==0)
                allFiture[i] = 1;
            else if (i<13 && i!=0)
                allFiture[i] = fitureHSI[i-1];
            else
                allFiture [i] = fitureGLCM[i-13];
            Log.e("All_Fitur", String.valueOf("Fitur : "+(i+1)+" "+allFiture[i]));
        }

        kelas = svm.getKelas(allFiture);
        if (kelas==1){
            kondisi.setText("Daging Segar");
            kondisi.setTextColor(getResources().getColor(R.color.colorSegar));
        }else {
            kondisi.setText("Daging Tidak Segar");
            kondisi.setTextColor(getResources().getColor(R.color.colorTidakSegar));
        }
//        Toast.makeText(this,"SVM :"+Double.toString(svm.getTmp()),Toast.LENGTH_LONG).show();

//        Log.e("GLCM", String.valueOf("Hasil : "));
//        Log.e("GLCM", String.valueOf("Con : "+glcm.getContrast()));
//        Log.e("GLCM", String.valueOf("Ent : "+glcm.getEntropy()));
//        Log.e("GLCM", String.valueOf("Ene : "+glcm.getEnergy()));
//        Log.e("GLCM", String.valueOf("Cor : "+glcm.getCorrelation()));
//        Log.e("GLCM", String.valueOf("Hom : "+glcm.getHomogenity()));
//        Log.e("GLCM", String.valueOf("Dis : "+glcm.getDissimilarity()));

//        Log.e("HSI", String.valueOf("Hasil : "));
//        Log.e("HSI", String.valueOf("Mean : "+hsi.getMeanq()));
//        Log.e("HSI", String.valueOf("Medi : "+hsi.getMedianq()));
//        Log.e("HSI", String.valueOf("Modu : "+hsi.getModusq()));
//        Log.e("HSI", String.valueOf("Vari : "+hsi.getVarianq()));
    }
}
