package com.example.alakadar;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    private Bitmap image;
    ImageView imageView;
    TextView entropy,energy,contras,correlation,dissimilarity,homogenity;
    Uri uri;
    android.support.v7.widget.Toolbar toolbar;
    File file;
    Intent camera,gallery,crop;
    Bitmap bitmap,grayBitmap;
    Mat rgba,grayMat;

    private float[]    c1, c2, c3;
    private float[]    rf, gf, bf;
    private int        size;
    private double[] HSI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        energy=(TextView)findViewById(R.id.Energy);
        entropy=(TextView)findViewById(R.id.Entropy);
        contras=(TextView)findViewById(R.id.Contras);
        dissimilarity=(TextView)findViewById(R.id.Dissimilarity);
        homogenity=(TextView)findViewById(R.id.Homogenity);
        correlation=(TextView)findViewById(R.id.Correlation);
        imageView = (ImageView) findViewById(R.id.imageView);

        if (!OpenCVLoader.initDebug())
            Log.d("ERROR", "Unable to load OpenCV");
        else
            Log.d("SUCCESS", "OpenCV loaded");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.tombolCamera){
            CameraOpen();
        }
        else if (item.getItemId()==R.id.tombolGallery){
            GalleryOpen();
        }
        return true;
    }

    private void GalleryOpen() {
        gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(gallery,"Select image from gallery"),2);
    }

    private void CameraOpen() {
        camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File(Environment.getExternalStorageDirectory(),
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        camera.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        camera.putExtra("return-data",true);
        startActivityForResult(camera,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==0 && resultCode==RESULT_OK){
            cropImage();
        }
        else if (requestCode==2){
            if (data != null){
                uri=data.getData();
                cropImage();
            }
        }
        else if (requestCode==1){
            if (data!=null){
                bitmap=(Bitmap)data.getExtras().get("data");
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    private void cropImage(){
        try {
            crop = new Intent("com.android.camera.action.CROP");
            crop.setDataAndType(uri,"image/*");

            crop.putExtra("crop","true");
            crop.putExtra("outputX",180);
            crop.putExtra("outputY",180);
            crop.putExtra("aspectX",4);
            crop.putExtra("aspectY",4);
            crop.putExtra("scaleUpIfNeeded",true);
            crop.putExtra("return-data",true);

            startActivityForResult(crop,1);
        }
        catch (ActivityNotFoundException ex){

        }
    }

    public void convert2Gray(View v){
        Mat rgba = new Mat();
        Mat grayMat = new Mat();
        int width=bitmap.getWidth();
        int height=bitmap.getHeight();
        grayBitmap=Bitmap.createBitmap(width,height,Bitmap.Config.RGB_565);
        Utils.bitmapToMat(bitmap,rgba);
        Imgproc.cvtColor(rgba,grayMat,Imgproc.COLOR_RGB2HSV);
        Utils.matToBitmap(grayMat,grayBitmap);
//        glcm(grayBitmap);
        imageView.setImageBitmap(getHSI(grayBitmap));
    }

    public void convert2Ycbcr(View v){
        Mat rgba = new Mat();
        Mat grayMat = new Mat();
        int width=bitmap.getWidth();
        int height=bitmap.getHeight();
        grayBitmap=Bitmap.createBitmap(width,height,Bitmap.Config.RGB_565);
        Utils.bitmapToMat(bitmap,rgba);
        Imgproc.cvtColor(rgba,grayMat,Imgproc.COLOR_RGB2YCrCb);
        Utils.matToBitmap(grayMat,grayBitmap);
        glcm(grayBitmap);
        imageView.setImageBitmap(grayBitmap);
    }
    public void glcm(Bitmap img){
            GLCM glcmfe = new GLCM(img,256);
            glcmfe.extract();
//            energy.setText("Ene : "+glcmfe.getEnergy());
//            entropy.setText("Ent : "+glcmfe.getEntropy());
//            contras.setText("Con : "+glcmfe.getContrast());
//            dissimilarity.setText("Dis : "+glcmfe.getDissimilarity());
//            homogenity.setText("Hom : "+glcmfe.getHomogenity());
//            correlation.setText("Cor : "+glcmfe.getCorrelation());
    }
    public Bitmap getHSI(Bitmap b){
//        rf = new float[size];
//        gf = new float[size];
//        bf = new float[size];
//        HSI= new double[size];
//        int offset, i;
//        for (int row = 0; row < b.getHeight(); row++){
//            offset = row*b.getWidth();
//            for (int col = 0; col < b.getWidth(); col++) {
//                i = offset + col;
//                int c = b.getPixel(col, row);
//                rf[i] = ((c&0xff0000)>>16)/255f;    //R 0..1
//                gf[i] = ((c&0x00ff00)>>8)/255f;     //G 0..1
//                bf[i] =  (c&0x0000ff)/255f;         //B 0..1
//                double numi = 1/2*((rf[i]-gf[i])+(rf[i]-bf[i]));
//                double denom = Math.pow((Math.pow((rf[i]-gf[i]), 2)+((rf[1]-bf[1])*(gf[1]-bf[1]))), 0.5);
//                double H=Math.acos(numi/(denom+0.000001));
//                if (bf[i]>gf[i]){
//                    H=360-H;
//                }
//                H = H/360;
//                double S = 1 - ((3/(rf[1]+gf[1]+bf[1]+0.000001))* Math.min(Math.min(rf[1], gf[1]),bf[1]));
//                double I = (rf[1]+gf[1]+bf[1])/3;
//                HSI[i]=H+S+I;
//            }
//        }
        for (int x = 0; x < b.getWidth(); ++x)
            for (int y = 0; y < b.getHeight(); ++y)
            {
                int rgb = b.getPixel(x, y);
                int R = (rgb >> 16) & 0xFF;
                int G = (rgb >> 8) & 0xFF;
                int B = (rgb & 0xFF);

                int numi = 1/2*((R-G)+(R-B));
                double denom = Math.pow((Math.pow((R-G), 2)+((R-B)*(R-B))), 0.5);
                double H = Math.acos(numi/denom);
                if (B>G){
                    H=360-H;
                }
                H = H/360;
                int S = 1 - ((3/(R+G+B))* Math.min(Math.min(R,G),B));
                int I = (R+G+B)/3;

                int HSI = ((int) H << 16) + (S << 8) + I;
                b.setPixel(x, y, HSI);
            }
        return b;
    }

}
