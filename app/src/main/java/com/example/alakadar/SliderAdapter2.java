package com.example.alakadar;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class SliderAdapter2 extends PagerAdapter {
    Context context;
    LayoutInflater layoutInflatert;

    ViewGroup vg;

    int pos =0;

    public SliderAdapter2(Context context){
        this.context= context;

    }

    //Array

    public int[] slide_images = {
            R.drawable.in1,
            R.drawable.in2,
            R.drawable.in3,
            R.drawable.in4,
            R.drawable.in5
    };

    public String[] slide_headings ={
            "sistem1",
            "sistem2",
            "sistem3",
            "sistem4",
            "sistem5"

    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (RelativeLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Log.e("cul", String.valueOf(position));
        layoutInflatert = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflatert.inflate(R.layout.activity_slider_adapter2, container, false);
        ImageView slideImageView = (ImageView) view.findViewById(R.id.slide_image);
        slideImageView.setImageResource(slide_images[position]);
        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}
